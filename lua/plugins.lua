local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({"git", "clone", "--filter=blob:none", "https://github.com/folke/lazy.nvim.git", "--branch=stable", lazypath,})
end
vim.opt.rtp:prepend(lazypath)

return require('lazy').setup({
  -- Related to files/find
  {
    'nvim-treesitter/nvim-treesitter',
    opts = {
      ensure_installed = {
        "bash",
        "c",
        "diff",
        "html",
        "javascript",
        "jsdoc",
        "json",
        "jsonc",
        "lua",
        "luadoc",
        "luap",
        "markdown",
        "markdown_inline",
        "printf",
        "python",
        "query",
        "regex",
        "toml",
        "tsx",
        "typescript",
        "vim",
        "vimdoc",
        "xml",
        "yaml",
      }
    }
  },
  { 'nvim-telescope/telescope.nvim', dependencies = { 'nvim-lua/plenary.nvim' } },
  {
    'simonmclean/triptych.nvim',
    event = 'VeryLazy',
    dependencies = {
      'nvim-lua/plenary.nvim',
      'nvim-tree/nvim-web-devicons',
      'antosha417/nvim-lsp-file-operations'
    },
    opts = {
      mappings = {
        nav_left = '<BS>',
        nav_right = '<CR>'
      }
    }
  },

  -- Related to git
  'airblade/vim-gitgutter',
  'tpope/vim-fugitive',

  -- Colorscheme
  'folke/lsp-colors.nvim',
  'morhetz/gruvbox',

  -- Completion
  'hrsh7th/cmp-nvim-lsp',
  'hrsh7th/cmp-buffer',
  'hrsh7th/cmp-path',
  'hrsh7th/cmp-cmdline',
  'hrsh7th/nvim-cmp',

  -- Generic LSP stuff
  'neovim/nvim-lspconfig',

  -- TeX support
  'lervag/vimtex',

  -- markdown preview
  'ellisonleao/glow.nvim',

  -- misc
  'matze/vim-move',

  -- AI support
  {
    "olimorris/codecompanion.nvim",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "nvim-treesitter/nvim-treesitter",
    },
    config = true,
    opts = {
      adapters = {
        deepSeek = function()
          return require("codecompanion.adapters").extend("ollama", {
            name = "deepSeek",
            schema = {
              model = {
                default = "deepseek-r1:8b"
              },
            }
          })
        end,
      },
      strategies = {
        chat = {
          adapter = "deepSeek",
        },
        inline = {
          adapter = "deepSeek",
        },
      },
    },
  },
})

