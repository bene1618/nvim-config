local cmd = vim.cmd
local create_augroup = require('util').create_augroup

create_augroup('filetype_java', {
  { 'FileType', 'java', 'setlocal', 'shiftwidth=4' },
  { 'FileType', 'java', 'setlocal', 'tabstop=4' }
})
