local create_augroup = require('util').create_augroup

local lspconfig = require('lspconfig')

-- Typescript language server
-- Install language server e.g. with `npm install -D --no-save @typescript/language-server`
lspconfig.ts_ls.setup {}

-- Rust language server
lspconfig.rust_analyzer.setup {}

-- Python language server
lspconfig.pyright.setup {}

-- Dart language server
lspconfig.dartls.setup {}

-- Clang language server
lspconfig.clangd.setup {}

-- Java language server
lspconfig.jdtls.setup {
  cmd = {
    "jdtls-lombok"
  }
}
