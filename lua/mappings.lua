local map = require('util').map

-- Map leader to space
vim.g.mapleader = ' '

-- General key mappings
map('n', '<leader>ev', '<cmd>vsplit ~/.config/nvim/lua/mappings.lua<CR>') -- edit init.lua
map('n', '<C-j>', '<C-]>') -- in my optinion easier to type and to remember for jumping to a link
map('v', '<C-c>', '"*y') -- yank selection to clipboard
map('n', '<C-c>', '<cmd>let @* = expand(\'%:p:h\')<CR>') -- copy current path to clipboard
map('n', '<ESC>', '<cmd>noh<CR><ESC>') -- unset the search highlighting by hitting ESC in normal mode
map('t', '<C-q>', '<C-\\><C-n>') -- leave terminal mode
map('n', '<C-w>d', '<cmd>windo diffthis<CR>') -- show diff between windows in tab
map('n', '<C-s>', '<C-i>') -- for some reasons <C-i> and <TAB> seem to be mapped to the same keys

-- Switching between tabs
map('n', '<TAB>', 'gt') -- move to next tab
map('n', '<S-TAB>', 'gT') -- move to previous tab

-- Quickfix list
map('n', '<F2>', '<cmd>cnext<CR>') -- move to next quickfix list entry
map('n', '<S-F2>', '<cmd>cprevious<CR>') -- move to previous quickfix list entry

-- LSP key mappings
map('n', 'dN', '<cmd>lua vim.diagnostic.goto_prev()<CR>')
map('n', 'dn', '<cmd>lua vim.diagnostic.goto_next()<CR>')
map('n', '<A-ENTER>', '<cmd>lua vim.lsp.buf.code_action()<CR>')
map('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>')
map('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<CR>')
map('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>')
map('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>')
map('n', '<leader>ff', '<cmd>lua vim.lsp.buf.formatting()<CR>')
map('n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>')
map('n', '<F6>', '<cmd>lua vim.lsp.buf.rename()<CR>')
map('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>')
map('n', 'gs', '<cmd>lua vim.lsp.buf.document_symbol()<CR>')

-- NeoTree plugin
map('n', '<C-n>', '<cmd>Triptych<CR>')

-- Telescope plugin
map('n', '<C-p>', '<cmd>Telescope find_files<CR>') -- Search for files
map('n', '<A-p>', '<cmd>Telescope live_grep<CR>') -- Live grep with Rg
map('n', '<F1>', '<cmd>Telescope help_tags<CR>') -- Show help tags instead of help entry point

-- Gitgutter plugin
map('n', ']h', '<cmd>GitGutterNextHunk<CR>')
map('n', '[h', '<cmd>GitGutterPrevHunk<CR>')
map('n', '<leader>hp', '<cmd>GitGutterPreviewHunk<CR>')
map('n', '<leader>hu', '<cmd>GitGutterUndoHunk<CR>')

-- Use <Tab> and <S-Tab> to navigate through popup menu
map('i', '<TAB>', 'pumvisible() ? "\\<C-n>" : "\\<Tab>"', { expr = true })
map('i', '<S-TAB>', 'pumvisible() ? "\\<C-p>" : "\\<Tab>"', { expr = true })

-- Vim-Test plugin
map('n', '<leader>tn', '<cmd>TestNearest<CR>')
map('n', '<leader>tf', '<cmd>TestFile<CR>')
map('n', '<leader>ts', '<cmd>TestSuite<CR>')
map('n', '<leader>tl', '<cmd>TestLast<CR>')
map('n', '<leader>tv', '<cmd>TestVisit<CR>')

-- indent-blankline plugin
map('n', '<leader>i', '<cmd>IBLToggle<CR>')

