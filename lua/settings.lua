local g = vim.g
local create_augroup = require('util').create_augroup
local opt = require('util').opt
local cmd = vim.cmd

-- General settings
local indent=2
opt('b', 'expandtab', true)
opt('b', 'smartindent', true)
opt('b', 'shiftwidth', indent)
opt('b', 'tabstop', indent)
opt('b', 'tw', 128)

opt('o', 'hidden', true)
opt('o', 'list', true)
opt('o', 'listchars', 'trail:-')

opt('w', 'number', true)
opt('w', 'cursorline', true)

-- LSP settings
create_augroup('lsp_highlight', {
  { 'CursorHold', '*', 'silent!', 'lua', 'vim.lsp.buf.document_highlight()' },
  { 'CursorHoldI', '*', 'silent!', 'lua', 'vim.lsp.buf.document_highlight()' },
  { 'CursorMoved', '*', 'silent!', 'lua', 'vim.lsp.buf.clear_references()' },
})
opt('o', 'updatetime', 300)

