local create_augroup = require('util').create_augroup

create_augroup('resize', {
  { 'VimResized', '*', 'wincmd =' }
})

create_augroup('detect_jenkinsfile_groovy', {
  { 'BufRead', 'Jenkinsfile*', 'set filetype=groovy' },
  { 'BufNewFile', 'Jenkinsfile*', 'set filetype=groovy' }
})

